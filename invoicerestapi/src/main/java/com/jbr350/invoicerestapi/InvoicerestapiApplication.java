package com.jbr350.invoicerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoicerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoicerestapiApplication.class, args);
		InvoiceItemController.getInvoiceList();
	}

}
