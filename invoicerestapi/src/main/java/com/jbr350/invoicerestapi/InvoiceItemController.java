package com.jbr350.invoicerestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping("/invoices")
    public static ArrayList<InvoiceItem> getInvoiceList() {
        InvoiceItem invoice1 = new InvoiceItem("1", "Paper", 2, 10000);
        InvoiceItem invoice2 = new InvoiceItem("2", "Pen", 3, 5000);
        InvoiceItem invoice3 = new InvoiceItem("3", "Book", 5, 25000);
        System.out.println("invoice1 la: " + invoice1);
        System.out.println("invoice2 la: " + invoice2);
        System.out.println("invoice3 la: " + invoice3);
        ArrayList<InvoiceItem> invoiceList = new ArrayList<InvoiceItem>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
    }
    
}
